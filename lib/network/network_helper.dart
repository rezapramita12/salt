import 'dart:convert';
import 'package:dio/dio.dart';

class NetworkHelper {
  static final Dio _dio = Dio();

  String _formDataToJson(FormData formData) {
    final fields = formData.fields;
    final files = formData.files;
    final map = <String, String>{};

    for (MapEntry<String, String> field in fields) {
      map[field.key] = field.value;
    }

    for (MapEntry<String, MultipartFile> file in files) {
      map[file.key] = file.value.filename!;
    }

    return json.encode(map);
  }

  Future<Response> get(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? progress,
      Options? options}) async {
    return await _dio.get(endpoint,
        queryParameters: params, onReceiveProgress: progress, options: options);
  }

  Future<Response> post(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? receiveProgress,
      Function(int, int)? progress,
      Options? options,
      dynamic data}) async {
    return await _dio.post(endpoint,
        queryParameters: params,
        onReceiveProgress: receiveProgress,
        options: options,
        onSendProgress: progress,
        data: data);
  }

  Future<Response> postFormData(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? receiveProgress,
      Function(int, int)? progress,
      Options? options,
      dynamic data}) async {
    return await _dio.post(endpoint,
        queryParameters: params,
        onReceiveProgress: receiveProgress,
        options: options,
        onSendProgress: progress,
        data: data);
  }

  Future<Response> put(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? receiveProgress,
      Function(int, int)? progress,
      Options? options,
      dynamic data}) async {
    return await _dio.put(endpoint,
        queryParameters: params,
        onReceiveProgress: receiveProgress,
        onSendProgress: progress,
        options: options,
        data: data);
  }

  Future<Response> delete(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Options? options,
      dynamic data}) async {
    return await _dio.delete(endpoint,
        queryParameters: params, options: options, data: data);
  }

  Future<Response> download(String endpoint,
      {bool isUseToken = true,
      savePath,
      dynamic data,
      Options? options,
      Function(int, int)? progress,
      Map<String, dynamic>? params}) async {
    return await _dio.download(endpoint, savePath,
        data: data,
        options: options,
        onReceiveProgress: progress,
        queryParameters: params);
  }
}
