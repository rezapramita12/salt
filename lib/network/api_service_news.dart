import 'package:dio/dio.dart';
import 'package:news_app/constant.dart';
import 'package:news_app/network/network_helper.dart';

class ApiServiceNews {
  final NetworkHelper _dio = NetworkHelper();

  Future<Response> getNews() async {
    final response = _dio.get(
      '${Constant().baseApi}/everything?q=bitcoin&apiKey=${Constant().apiKey}',
    );
    return response;
  }
}
