// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:news_app/constant.dart';
import 'package:news_app/models/response_news.dart';

class DetailNews extends StatefulWidget {
  final Article data;
  const DetailNews({Key? key, required this.data}) : super(key: key);

  @override
  State<DetailNews> createState() => _DetailNewsState();
}

class _DetailNewsState extends State<DetailNews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.data.title!,
          style: const TextStyle(color: Colors.black),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: const [],
      ),
      body: Container(
        color: Colors.white,
        width: double.infinity,
        height: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  height: 400,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.2),
                  ),
                  child: ClipRRect(
                    child: CachedNetworkImage(
                      imageUrl: "${widget.data.urlToImage}",
                      placeholder: (context, url) => const Center(
                          child: CircularProgressIndicator(
                        color: Colors.amber,
                      )),
                      errorWidget: (context, url, error) => SizedBox(
                        height: 200,
                        child: Image.asset(
                          'assets/postingan.png',
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                        ),
                      ),
                      fit: BoxFit.cover,
                      width: double.maxFinite,
                    ),
                  )),
              const Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(Constant()
                            .dateFormat
                            .format(widget.data.publishedAt!)
                            .toString()),
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      '${widget.data.title}',
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        '${widget.data.description}',
                        style: const TextStyle(fontSize: 13),
                      ),
                    ),
                    const SizedBox(
                      height: 16.0,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
