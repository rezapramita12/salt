// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:news_app/bloc/news/news_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_app/constant.dart';
import 'package:news_app/global_widget.dart';
import 'package:news_app/models/response_news.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:news_app/ui/detail_news.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<NewsBloc>().add(GetNews());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "News",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: const [],
      ),
      body: BlocConsumer<NewsBloc, NewsState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          if (state is NewsLoading) {
            return GlobalWidget().skeletonList();
          } else if (state is NewsSuccess) {
            if (state.responseNews.articles!.isEmpty) {
              return const Center(
                child: Text('Data tidak tersedia'),
              );
            } else {
              return ListView.separated(
                separatorBuilder: (context, index) => const SizedBox(
                  height: 16,
                ),
                itemCount: state.responseNews.articles!.length,
                physics: const ScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  Article data = state.responseNews.articles![index];
                  return GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailNews(data: data))),
                    child: Column(
                      children: [
                        Container(
                            height: 400,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.2),
                            ),
                            child: ClipRRect(
                              child: CachedNetworkImage(
                                imageUrl: "${data.urlToImage}",
                                placeholder: (context, url) => const Center(
                                    child: CircularProgressIndicator(
                                  color: Colors.amber,
                                )),
                                errorWidget: (context, url, error) => SizedBox(
                                  height: 200,
                                  child: Image.asset(
                                    'assets/postingan.png',
                                    fit: BoxFit.cover,
                                    width: double.maxFinite,
                                  ),
                                ),
                                fit: BoxFit.cover,
                                width: double.maxFinite,
                              ),
                            )),
                        const Divider(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(Constant()
                                      .dateFormat
                                      .format(data.publishedAt!)
                                      .toString()),
                                ],
                              ),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                '${data.title}',
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              const SizedBox(
                                height: 10.0,
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  '${data.description}',
                                  style: const TextStyle(fontSize: 13),
                                ),
                              ),
                              const SizedBox(
                                height: 16.0,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            }
          } else if (state is NewsError) {
            return Center(
              child: ElevatedButton(
                  onPressed: () {
                    context.read<NewsBloc>().add(GetNews());
                  },
                  child: const Text("Coba Lagi")),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
