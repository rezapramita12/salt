import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:news_app/models/response_news.dart';
import 'package:news_app/network/api_service_news.dart';

part 'news_event.dart';
part 'news_state.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  final ApiServiceNews _apiServiceNews = ApiServiceNews();
  NewsBloc() : super(NewsInitial()) {
    on<GetNews>((event, emit) async {
      emit(NewsLoading());
      try {
        final response = await _apiServiceNews.getNews();
        emit(NewsSuccess(ResponseNews.fromJson(response.data)));
      } on DioError catch (e) {
        emit(NewsError(e.response!.data['message']));
      } catch (e) {
        emit(NewsError(e.toString()));
      }
      // TODO: implement event handler
    });
  }
}
