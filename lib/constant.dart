import 'package:intl/intl.dart';

class Constant {
  String apiKey = '77a28551080442cf9447ba7f2b169cf4';
  String baseApi = 'https://newsapi.org/v2';
  DateFormat dateFormat = DateFormat("dd MMMM yyyy");
}
