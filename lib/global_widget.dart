import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class GlobalWidget {
  skeletonList() {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Shimmer.fromColors(
              baseColor: Colors.grey.withOpacity(0.2),
              highlightColor: Colors.white,
              period: const Duration(milliseconds: 800),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Container(
                            width: 100,
                            height: 10,
                            color: Colors.grey,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: 100,
                            height: 10,
                            color: Colors.grey,
                          ),
                        ],
                      ),
                      Container(
                        width: 100,
                        height: 10,
                        color: Colors.grey,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Container(
                    width: double.infinity,
                    height: 66,
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
          );
        });
  }
}
